[Version 0.0.4 (2024-10-29)](https://pypi.org/project/watchpylint/0.0.4/)
============================

* Move watchpylint script into a module and add entry point config ([585cfb8](https://gitlab.com/ktpanda/watchpylint/-/commit/585cfb865cb278bf39bfb1980b611d954db931c6))


[Version 0.0.3 (2022-12-02)](https://pypi.org/project/watchpylint/0.0.3/)
============================

* Add "<lint complete>" to end of text so `watchpylint --wait` can detect an incomplete file ([2c4e85a](https://gitlab.com/ktpanda/watchpylint/-/commit/2c4e85a2a14eac31e7082d258fde3a24529504fc))


[Version 0.0.2 (2022-10-07)](https://pypi.org/project/watchpylint/0.0.2/)
============================

* Print file paths relative to the directory that watchpylint is invoked from ([ea0c5a6](https://gitlab.com/ktpanda/watchpylint/-/commit/ea0c5a690af758cc862c069d479e67d0b03ef9b6))


[Version 0.0.1 (2022-10-05)](https://pypi.org/project/watchpylint/0.0.1/)
============================

* Initial commit ([7265c6e](https://gitlab.com/ktpanda/watchpylint/-/commit/7265c6e7ff4b6ab760b805992f29d6913f2da723))
